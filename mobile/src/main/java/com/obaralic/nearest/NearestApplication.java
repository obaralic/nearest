package com.obaralic.nearest;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.obaralic.nearest.parse.object.ParseLocation;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;

/**
 * Created by Home on 7/4/2015.
 */
public class NearestApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        final String applicationId = getString(R.string.parse_app_id);
        final String clientKey = getString(R.string.parse_client_key);

        // Enable Local Datastore.
        // Parse.enableLocalDatastore(this);

        ParseObject.registerSubclass(ParseLocation.class);
        Parse.initialize(this, applicationId, clientKey);
        ParseInstallation.getCurrentInstallation().saveInBackground();

        SharedPreferences prefs = getSharedPreferences("TAG", Context.MODE_PRIVATE);
        prefs.edit().putFloat("radius", 200.0f).commit();
    }
}
