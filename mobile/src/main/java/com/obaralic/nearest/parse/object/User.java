package com.obaralic.nearest.parse.object;

import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseUser;

/**
 * Created by zivorad on 7/15/15.
 */
public class User {

    public static final String KEY_USER = "user";

    public static final String KEY_PHONE_NUMBER = "phoneNumber";

    public static final String KEY_PHOTO = "photo";

    public static final String KEY_PHOTO_FILE = "file";

    public static final String KEY_USERNAME = "username";

    public static final String KEY_GEO_POINT = "geoPoint";

    public static final String KEY_NAME = "name";

    private ParseUser mParseUser;

    public User(ParseUser user) {
        mParseUser = user;
    }

    public static User getCurrentUser() {
        ParseUser parseUser = ParseUser.getCurrentUser();
        User user = parseUser != null ? new User(parseUser) : null;
        return user;
    }

    public void setPhoto(byte[] photo) {
        mParseUser.put(KEY_PHOTO, photo);
    }

    public void setImageFile(ParseFile file) {
        mParseUser.put(KEY_PHOTO_FILE, file);
    }

    public void setPhoneNumber(String phoneNumber) {
        mParseUser.put(KEY_PHONE_NUMBER, phoneNumber);
    }

    public void setName(String name) {
        mParseUser.put(KEY_NAME, name);
    }

    public void setGeoPoint(double latitude, double longitude) {
        mParseUser.put(KEY_GEO_POINT, new ParseGeoPoint(latitude, longitude));
    }

    public String getPhoneNumber() {
        return mParseUser.getString(KEY_PHONE_NUMBER);
    }

    public String getName() {
        return mParseUser.getString(KEY_NAME);
    }

    public String getUsername() {
        return mParseUser.getString(KEY_USERNAME);
    }

    public byte[] getPhoto() {
        return mParseUser.getBytes(KEY_PHOTO);
    }

    public ParseFile getImageFile() {
        return mParseUser.getParseFile(KEY_PHOTO_FILE);
    }

    public ParseGeoPoint getGeoPoint() {
        return mParseUser.getParseGeoPoint(KEY_GEO_POINT);
    }

    public void saveInBackground() {
        mParseUser.saveInBackground();
    }

    public ParseUser getParseUser() {
        return mParseUser;
    }

    public String getObjectId() {
        return mParseUser.getObjectId();
    }
}
