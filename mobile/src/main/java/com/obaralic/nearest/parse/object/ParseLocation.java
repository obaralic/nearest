package com.obaralic.nearest.parse.object;

import com.parse.ParseClassName;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;

import android.location.Location;

/***/
@ParseClassName("ParseLocation")
public class ParseLocation extends ParseObject {

    public static final String KEY_GEO_POINT = "geoPoint";

    public static final String KEY_ACCURACY = "accuracy";

    public static final String KEY_SPEED = "speed";

    public static final String KEY_ALTITUDE = "altitude";

    public static final String KEY_BEARING= "bearing";

    public static final String KEY_USER = "user";

    public ParseLocation() {
    }

    public void initialize(Location location) {
        setAccuracy(location.getAccuracy());
        setSpeed(location.getSpeed());
        setAltitude(location.getAltitude());
        setBearing(location.getBearing());
        setGeoPoint(location.getLatitude(), location.getLongitude());
    }

    public void setAccuracy(double accuracy) {
        this.put(KEY_ACCURACY, accuracy);
    }

    public void setAltitude(double altitude) {
        this.put(KEY_ALTITUDE, altitude);
    }

    public void setSpeed(double speed) {
        this.put(KEY_SPEED, speed);
    }

    public void setBearing(double bearing) {
        this.put(KEY_BEARING, bearing);
    }

    public void setGeoPoint(double latitude, double longitude) {
        this.put(KEY_GEO_POINT, new ParseGeoPoint(latitude, longitude));
    }

    public void setUser(ParseUser user) {
        this.put(KEY_USER, user);
    }

    public double getAltitude() {
        return this.getDouble(KEY_ALTITUDE);
    }

    public double getSpeed() {
        return this.getDouble(KEY_SPEED);
    }

    public double getBearing() {
        return this.getDouble(KEY_BEARING);
    }

    public double getAccuracy() {
        return this.getDouble(KEY_ACCURACY);
    }

    public ParseGeoPoint getGeoPoint() {
        return this.getParseGeoPoint(KEY_GEO_POINT);
    }

    public ParseUser getUser() {
        return this.getParseUser(KEY_USER);
    }
}
