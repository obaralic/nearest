package com.obaralic.nearest.parse;

import com.obaralic.nearest.parse.object.User;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * Created by zivorad on 7/24/15.
 */
public class Push {

    public static void associateDeviceWithUser() {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put(User.KEY_USER, ParseUser.getCurrentUser());
        installation.saveInBackground();
    }

    public static synchronized void sendDirectMessage(ParseUser user, String message) {
        // Create our Installation query
        ParseQuery pushQuery = ParseInstallation.getQuery();
        pushQuery.whereEqualTo(User.KEY_USER, user);

        // Send push notification to query
        ParsePush push = new ParsePush();
        push.setQuery(pushQuery);
        push.setMessage(message);
        push.sendInBackground();
    }

    public static synchronized void sendGeoLocalizedMessage(ParseGeoPoint center,
            double radiusInKm, String message) {
        // Find users near a given location
        ParseQuery userQuery = ParseUser.getQuery();
        userQuery.whereWithinKilometers(User.KEY_GEO_POINT, center, radiusInKm);
        userQuery.whereNotEqualTo(User.KEY_USERNAME, ParseUser.getCurrentUser().getUsername());

        // Find devices associated with these users
        ParseQuery pushQuery = ParseInstallation.getQuery();
        pushQuery.whereMatchesQuery(User.KEY_USER, userQuery);

        // Send push notification to query
        ParsePush push = new ParsePush();
        push.setQuery(pushQuery);
        push.setMessage(message);
        push.sendInBackground();
    }
}
