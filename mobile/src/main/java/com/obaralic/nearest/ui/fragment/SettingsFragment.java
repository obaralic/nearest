package com.obaralic.nearest.ui.fragment;

import com.obaralic.nearest.R;
import com.obaralic.nearest.parse.Push;
import com.obaralic.nearest.parse.object.User;
import com.parse.ParseGeoPoint;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by zivorad on 7/16/15.
 */
public class SettingsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        final ParseGeoPoint center = User.getCurrentUser().getGeoPoint();
        final SharedPreferences prefs = getActivity().getSharedPreferences("TAG", Context
            .MODE_PRIVATE);
        final double radius = prefs.getFloat("radius", 500);
        final String message = "Greetings from " + User.getCurrentUser().getUsername();
        Push.sendGeoLocalizedMessage(center, radius, message);
    }
}
