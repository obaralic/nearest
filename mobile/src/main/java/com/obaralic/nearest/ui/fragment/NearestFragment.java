package com.obaralic.nearest.ui.fragment;

import com.obaralic.nearest.R;
import com.obaralic.nearest.parse.Push;
import com.obaralic.nearest.parse.object.User;
import com.parse.ParseGeoPoint;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by zivorad on 7/14/15.
 */
public class NearestFragment extends ListFragment implements AdapterView.OnItemClickListener {

    private static final String TAG = NearestFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_nearest, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initParseAdapter(view.getContext(), 10);
        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        final ParseUser parseUser = (ParseUser) view.getTag();

        Push.sendDirectMessage(parseUser, "Ola chiquita como est as?");

        final User user = new User(parseUser);
        final String toast = "Username: " + parseUser.getUsername() + "\nEmail: " + parseUser
            .getEmail() + "\nName: " + user.getName() + "\nNumber: " + user.getPhoneNumber();
        Toast.makeText(getActivity(), toast, Toast.LENGTH_LONG).show();
    }

    private void initParseAdapter(final Context context, final int count) {
        ParseQueryAdapter.QueryFactory<ParseUser> factory =
                new ParseQueryAdapter.QueryFactory<ParseUser>() {

            @Override
            public ParseQuery<ParseUser> create() {
                return buildNearestUsersQuery(count);
            }
        };

        CustomQueryAdapter adapter = new CustomQueryAdapter(context, factory);
        adapter.setTextKey(User.KEY_USERNAME);
        adapter.setImageKey(User.KEY_PHOTO_FILE);
        setListAdapter(adapter);
    }

    private ParseQuery<ParseUser> buildNearestUsersQuery(final int count) {
        final ParseUser owner = User.getCurrentUser().getParseUser();
        final ParseGeoPoint userLocation = User.getCurrentUser().getGeoPoint();
        final ParseQuery query = new ParseQuery(ParseUser.class);
        query.whereNear(User.KEY_GEO_POINT, userLocation);
        query.whereNotEqualTo(User.KEY_USERNAME, owner.getUsername());
        query.setLimit(count);
        return query;
    }

    private class CustomQueryAdapter extends ParseQueryAdapter<ParseUser> {

        public CustomQueryAdapter(Context context, QueryFactory queryFactory) {
            super(context, queryFactory);
        }

        @Override
        public View getItemView(ParseUser user, View view, ViewGroup parent) {
            if (view == null) {
                view = View.inflate(getContext(), R.layout.layout_nearest_list_item, null);
            }
            view.setTag(user);

            super.getItemView(user, view, parent);

            // Do additional configuration before returning the View.
            TextView descriptionView = (TextView) view.findViewById(android.R.id.text2);
            descriptionView.setText(user.getParseGeoPoint(User.KEY_GEO_POINT).toString());
            return view;
        }


        @Override
        public View getNextPageView(View v, ViewGroup parent) {
            return super.getNextPageView(v, parent);
        }
    }
}
