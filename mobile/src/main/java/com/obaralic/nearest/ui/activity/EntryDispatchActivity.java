package com.obaralic.nearest.ui.activity;

import com.obaralic.nearest.parse.Push;
import com.parse.ui.ParseLoginDispatchActivity;

/**
 * Created by zivorad on 7/13/15.
 */
public class EntryDispatchActivity extends ParseLoginDispatchActivity {

    @Override
    protected Class<?> getTargetClass() {
        Push.associateDeviceWithUser();
        return MainActivity.class;
    }
}
