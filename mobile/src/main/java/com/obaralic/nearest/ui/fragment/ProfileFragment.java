package com.obaralic.nearest.ui.fragment;

import com.obaralic.nearest.R;
import com.obaralic.nearest.parse.object.User;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;

import android.app.Activity;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

/**
 * Created by zivorad on 7/14/15.
 */
public class ProfileFragment extends Fragment implements View.OnClickListener{

    private static final int CAMERA_REQUEST = 1888;

    private static final int CROP_REQUEST = 1889;

    private ImageView mIconView;

    private Button mEditOrSaveButton;

    private EditText mUsernameEditText;
    private EditText mNameEditText;
    private EditText mEmailEditText;
    private EditText mPhoneEditText;

    private boolean mIsEditing = false;

    private User mUser = User.getCurrentUser();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_REQUEST) {
                Uri iconUri = data.getData();
                performCrop(iconUri);

            } else if (requestCode == CROP_REQUEST) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                mIconView.setImageBitmap(photo);

                final String iconName = mUser.getParseUser().getUsername() + ".png";
                final byte[] iconBytes = bitmapToBytes(photo);
                ParseFile iconFile = new ParseFile(iconName, iconBytes);
                mUser.setImageFile(iconFile);
                mUser.saveInBackground();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.userIcon:
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                break;

            case R.id.profileEditButton:
                mIsEditing = !mIsEditing;
                if (mIsEditing) {
                    mEditOrSaveButton.setText("Save");

                    mIconView.setOnClickListener(this);
                    mPhoneEditText.setEnabled(true);
                    mNameEditText.setEnabled(true);

                    mNameEditText.setText(mUser.getName());
                    mPhoneEditText.setText(mUser.getPhoneNumber());

                } else {
                    mEditOrSaveButton.setText("Edit");

                    mIconView.setOnClickListener(null);
                    mPhoneEditText.setEnabled(false);
                    mNameEditText.setEnabled(false);

                    String name = mNameEditText.getText().toString();
                    String phone = mPhoneEditText.getText().toString();
                    mUser.setName(name);
                    mUser.setPhoneNumber(phone);
                    mUser.saveInBackground();
                }
                break;
        }
    }

    private void initViews(View view) {
        mIconView = (ImageView) view.findViewById(R.id.userIcon);
        mEditOrSaveButton = (Button) view.findViewById(R.id.profileEditButton);
        mUsernameEditText = (EditText) view.findViewById(R.id.usernameEditText);
        mNameEditText = (EditText) view.findViewById(R.id.nameEditText);
        mEmailEditText = (EditText) view.findViewById(R.id.emailEditText);
        mPhoneEditText = (EditText) view.findViewById(R.id.phoneEditText);

        mEditOrSaveButton.setOnClickListener(this);

        User user = User.getCurrentUser();
        ParseUser parseUser = user.getParseUser();
        mNameEditText.setText(user.getName());
        mPhoneEditText.setText(user.getPhoneNumber());
        mUsernameEditText.setText(parseUser.getUsername());
        mEmailEditText.setText(parseUser.getEmail());

        ParseFile iconFile = user.getImageFile();
        if (iconFile != null) {
            iconFile.getDataInBackground(new GetDataCallback() {

                @Override
                public void done(byte[] bytes, ParseException e) {
                    Bitmap icon = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    mIconView.setImageBitmap(icon);
                }
            });
        }
    }

    private void saveUserIcon() {
        byte[] byteArray = bitmapToBytes(BitmapFactory.decodeResource(getResources(),
            R.drawable.ic_launcher));

        User user = User.getCurrentUser();
        user.setPhoto(byteArray);
        user.saveInBackground();
    }

    private byte[] bitmapToBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    private void performCrop(Uri uri){
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(uri, "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 128);
            cropIntent.putExtra("outputY", 128);
            cropIntent.putExtra("return-data", true);
            startActivityForResult(cropIntent, CROP_REQUEST);

        } catch(ActivityNotFoundException anfe){
            String errorMessage = "Your device doesn't support the crop action!";
            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
