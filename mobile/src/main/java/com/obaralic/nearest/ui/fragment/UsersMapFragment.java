package com.obaralic.nearest.ui.fragment;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.obaralic.nearest.R;
import com.obaralic.nearest.parse.object.User;
import com.obaralic.nearest.service.GpsService;
import com.obaralic.nearest.utils.GeoUtils;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PointF;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by zivorad on 7/14/15.
 */
public class UsersMapFragment extends Fragment implements OnMapReadyCallback, View.OnClickListener {

    private GoogleMap mGoogleMap;

    private Button mButton;

    private int mMapType = 0;

    private boolean mIsReady = false;

    private GeoUtils mGeoUtils = new GeoUtils();

    private Circle mDetectionArea = null;

    private List<MarkerOptions> mMarkerOptionsList = new ArrayList<>();

    private List<User> mUsers = new ArrayList<>();

    private HashMap<String, Bitmap> mIconCache = new HashMap<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        mButton = (Button) rootView.findViewById(R.id.mapTypeButton);
        mButton.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MapFragment mapFragment = (MapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        ParseGeoPoint[] geoBox = mGeoUtils.getGeoBoxCoordinates(getActivity(),
            User.getCurrentUser().getGeoPoint());
        locateNearestUsers(geoBox[0], geoBox[1]);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(mLocationUpdateReceiver,
            new IntentFilter(GpsService.ACTION_LOCATION_UPDATE));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mLocationUpdateReceiver);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        final Context context = getActivity().getBaseContext();
        mGoogleMap = googleMap;
        mIsReady = true;

        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        ParseGeoPoint point = User.getCurrentUser().getGeoPoint();
        LatLng userLatLng = new LatLng(point.getLatitude(), point.getLongitude());
        updateCamera(googleMap, userLatLng, true);
        drawScanArea(context, googleMap, userLatLng);
    }

    @Override
    public void onClick(View view) {
        if (!mIsReady) return;
        switch (view.getId()) {
            case R.id.mapTypeButton:
                updateButton();
                break;
        }
    }

    private void updateButton() {
        mMapType = (mMapType + 1) % 3;
        switch (mMapType) {
            case 0:
                mButton.setText("SATELLITE");
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case 1:
                mButton.setText("HYBRID");
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case 2:
                mButton.setText("NORMAL");
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
        }
    }

    private void locateNearestUsers(final ParseGeoPoint southWest, final ParseGeoPoint northEast) {
        User user = User.getCurrentUser();
        ParseQuery<ParseUser> query = ParseQuery.getQuery(ParseUser.class);
        query.whereNotEqualTo(User.KEY_USERNAME, user.getUsername());
        query.whereWithinGeoBox(User.KEY_GEO_POINT, southWest, northEast);
        query.findInBackground(new FindCallback<ParseUser>() {

            @Override
            public void done(List<ParseUser> list, ParseException e) {
                mUsers.clear();
                for (ParseUser user : list) {
                    mUsers.add(new User(user));
                }
                drawUsersOnMap();
            }
        });
    }

    private void drawScanArea(Context context, GoogleMap googleMap, LatLng location) {
        if (mDetectionArea != null) mDetectionArea.remove();

        SharedPreferences prefs = context.getSharedPreferences("TAG", Context.MODE_PRIVATE);
        float radius = prefs.getFloat("radius", 0);

        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(location);
        circleOptions.radius(radius);
        circleOptions.strokeWidth(6f);
        circleOptions.strokeColor(context.getResources().getColor(android.R.color.holo_blue_dark));
        mDetectionArea = googleMap.addCircle(circleOptions);

    }

    private void updateCamera(GoogleMap map, LatLng location, boolean isAnimated) {
        final float zoom = map.getCameraPosition().zoom;
        CameraPosition position = CameraPosition.builder().target(location).zoom(zoom).build();
        CameraUpdate update = CameraUpdateFactory.newCameraPosition(position);
        if (isAnimated) {
            map.animateCamera(update, 500, null);
        } else {
            map.moveCamera(update);
        }
    }

    private void drawUsersOnMap() {
        mMarkerOptionsList.clear();
        for (User user : mUsers) {
            final String userId = user.getObjectId();
            ParseGeoPoint point = user.getGeoPoint();
            MarkerOptions marker = new MarkerOptions();
            marker.position(new LatLng(point.getLatitude(), point.getLongitude()));
            marker.title(user.getName() != null ? user.getName() : user.getUsername());

            Bitmap cachedIcon = mIconCache.get(user.getObjectId());
            if (cachedIcon == null) {
                try {
                    byte[] bytes = user.getImageFile().getData();
                    cachedIcon = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    mIconCache.put(userId, cachedIcon);
                } catch (ParseException e) {
                }
            }

            marker.icon(BitmapDescriptorFactory.fromBitmap(cachedIcon));

            mMarkerOptionsList.add(marker);
        }
        if (!mIsReady) return;
        for (MarkerOptions marker : mMarkerOptionsList) {
            mGoogleMap.addMarker(marker);
        }
    }

    private final BroadcastReceiver mLocationUpdateReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (!mIsReady) return;
            Location location = intent.getExtras().getParcelable(GpsService.EXTRA_LOCATION_UPDATE);
            final double lat = location.getLatitude();
            final double lng = location.getLongitude();
            mGoogleMap.clear();

            LatLng latLng = new LatLng(lat, lng);
            drawScanArea(context, mGoogleMap, latLng);
            updateCamera(mGoogleMap, latLng, false);
            drawUsersOnMap();

            ParseGeoPoint geoPoint = new ParseGeoPoint(lat, lng);
            ParseGeoPoint[] geoBox = mGeoUtils.getGeoBoxCoordinates(context, geoPoint);
            locateNearestUsers(geoBox[0], geoBox[1]);
        }
    };
}
