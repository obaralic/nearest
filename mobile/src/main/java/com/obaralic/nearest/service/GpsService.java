package com.obaralic.nearest.service;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.obaralic.nearest.R;
import com.obaralic.nearest.broadcast.ConnectionChangeReceiver;
import com.obaralic.nearest.parse.object.ParseLocation;
import com.obaralic.nearest.parse.object.User;
import com.obaralic.nearest.ui.activity.MainActivity;
import com.obaralic.nearest.utils.GeoUtils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

/**
 * Created by zivorad on 7/10/15.
 */
public class GpsService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, ResultCallback<Status> {

    public static final String EXTRA_EXECUTE_AND_STOP = "executeAndStop";

    public static final String EXTRA_LOCATION_UPDATE = "locationUpdate";

    public static final String ACTION_LOCATION_UPDATE =
        "com.obaralic.nearest.service.ACTION_LOCATION_UPDATE";

    private static final String TAG = GpsService.class.getSimpleName();

    private static final int MINUTES = 2;

    private static final int UPDATE_INTERVAL_TIME = MINUTES * 60 * 1000;

    private static final int UPDATE_INTERVAL_DISTANCE = 50;

    private GoogleApiClient mGoogleApiClient;

    private LocationRequest mLocationRequest;

    private AlarmManager mAlarmManager;

    private PendingIntent alarmIntent;

    private boolean mIsExecuteAndStop;

    private ParseLocation mParseLocation;

    public static Intent getIntent(Context context) {
        final Intent intent = new Intent(context.getApplicationContext(), GpsService.class);
        intent.putExtra(GpsService.EXTRA_EXECUTE_AND_STOP, false);
        return  intent;
    }

    public GpsService() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "called onCreate");
        buildGoogleApiClient();
        if (!mGoogleApiClient.isConnecting() || !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "called onDestroy");
        if (mGoogleApiClient.isConnecting() || mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "called onStartCommand");
        mIsExecuteAndStop = intent.getBooleanExtra(EXTRA_EXECUTE_AND_STOP, false);
        return START_NOT_STICKY;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "called onConnected");

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL_TIME);
        mLocationRequest.setSmallestDisplacement(UPDATE_INTERVAL_DISTANCE);

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation
            (mGoogleApiClient);
    }

    @Override
    public void onLocationChanged(Location location) {
        saveToCloud(location);
        locateNearestUsers();
        sendLocationUpdate(location);
        if (mIsExecuteAndStop) {
            stopSelf();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "called onConnectionSuspended: " + i);
        // mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "called onConnectionFailed: " + connectionResult);
    }

    @Override
    public void onResult(Status status) {
        if (!status.isSuccess()) {
            stopSelf();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void sendLocationUpdate(Location location) {
        Intent broadcast = new Intent(ACTION_LOCATION_UPDATE);
        broadcast.putExtra(EXTRA_LOCATION_UPDATE, location);
        sendBroadcast(broadcast);
    }

    private void createAlarm() {
        final Intent intent = new Intent(ConnectionChangeReceiver.ACTION_INVOKE_SERVICE);
        alarmIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        mAlarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                AlarmManager.INTERVAL_FIFTEEN_MINUTES, AlarmManager.INTERVAL_FIFTEEN_MINUTES,
                alarmIntent);
    }

    private void buildGoogleApiClient() {
        GoogleApiClient.Builder builder = new GoogleApiClient.Builder(this);
        builder.addApi(LocationServices.API);
        builder.addConnectionCallbacks(this);
        builder.addOnConnectionFailedListener(this);
        mGoogleApiClient = builder.build();
    }

    private void saveToCloud(final Location location) {
        final User user = User.getCurrentUser();
        if (user == null) {
            Toast.makeText(getApplicationContext(), "NOT SIGNED IN!", Toast.LENGTH_LONG).show();
            return;
        }

        // Save current user location within user object.
        user.setGeoPoint(location.getLatitude(), location.getLongitude());
        user.saveInBackground();

        // Save current user location within ParseLocation object associated with user object.
        if (mParseLocation != null) {
            mParseLocation.initialize(location);
            mParseLocation.saveInBackground();
        } else {
            ParseQuery<ParseLocation> query = ParseQuery.getQuery(ParseLocation.class);
            query.whereEqualTo(ParseLocation.KEY_USER, user.getParseUser());
            query.findInBackground(new FindCallback<ParseLocation>() {
                @Override
                public void done(List<ParseLocation> list, ParseException error) {
                    if (list == null || list.isEmpty()) {
                        mParseLocation = new ParseLocation();
                        mParseLocation.setUser(user.getParseUser());
                    } else {
                        mParseLocation = list.get(0);
                    }

                    mParseLocation.initialize(location);
                    mParseLocation.saveInBackground();
                    Log.d(TAG, "Saved to cloud!");
                }
            });
        }
    }

    private void locateNearestUsers() {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                User user = User.getCurrentUser();
                if (user == null) return null;
                GeoUtils geoUtils = new GeoUtils();
                ParseGeoPoint[] geoBox = geoUtils.getGeoBoxCoordinates(getApplicationContext(),
                    user.getGeoPoint());
                locateNearestUsers(geoBox[0], geoBox[1]);
                return null;
            }
        }.execute();
    }

    private void locateNearestUsers(final ParseGeoPoint southWest, final ParseGeoPoint northEast) {
        User currentUser = User.getCurrentUser();
        ParseQuery<ParseUser> query = ParseQuery.getQuery(ParseUser.class);
        query.whereNotEqualTo(User.KEY_USERNAME, currentUser.getUsername());
        query.whereWithinGeoBox(User.KEY_GEO_POINT, southWest, northEast);
        List<ParseUser> list = null;
        try {
            list = query.find();
            if (list.isEmpty()) return;
        } catch (ParseException e) {
            e.printStackTrace();
            return;
        }

        User nerestUser = new User(list.get(0));
        String title = "Someone is near you!";
        String text = nerestUser.getUsername() + "\n" + nerestUser.getName();

        Bitmap icon = null;
        try {
            byte[] data = nerestUser.getImageFile().getData();
            icon = BitmapFactory.decodeByteArray(data, 0, data.length);
        } catch (ParseException e) {
        }

        sendNotification(title, text, icon);
    }

    private void sendNotification(String title, String text, Bitmap icon) {
        // Create an explicit content Intent that starts the main Activity.
        Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
        // Construct a task stack.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Add the main Activity to the task stack as the parent.
        stackBuilder.addParentStack(MainActivity.class);
        // Push the content Intent onto the stack.
        stackBuilder.addNextIntent(notificationIntent);
        // Get a PendingIntent containing the entire back stack.
        PendingIntent notificationPendingIntent =
            stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        // Get a notification builder that's compatible with platform versions >= 4
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        // Define the notification settings.
        builder.setSmallIcon(R.drawable.ic_launcher)
            .setLargeIcon(icon)
            .setContentTitle(title)
            .setContentText(text)
            .setContentIntent(notificationPendingIntent);
        // Dismiss notification once the user touches it.
        builder.setAutoCancel(true);
        // Get an instance of the Notification manager
        NotificationManager mNotificationManager =
            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // Issue the notification
        mNotificationManager.notify(0, builder.build());
    }
}
