package com.obaralic.nearest.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.obaralic.nearest.service.GpsService;

/***/
public class ConnectionChangeReceiver extends BroadcastReceiver {

    private static final String TAG = ConnectionChangeReceiver.class.getSimpleName();

    public static final String ACTION_INVOKE_SERVICE =
            "com.obaralic.nearest.broadcast.ACTION_INVOKE_SERVICE";

    @Override
    public void onReceive(Context context, Intent intent) {
        final ConnectivityManager manager =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        final NetworkInfo network = manager.getActiveNetworkInfo();
        final boolean isConnected = network != null && network.isConnectedOrConnecting();
        Log.d(TAG, intent.getAction() + " is connected: " + isConnected);

        final Intent serviceIntent = new Intent(context, GpsService.class);
        if (isConnected) {
            intent.putExtra(GpsService.EXTRA_EXECUTE_AND_STOP, true);
            context.startService(serviceIntent);
        } else {
            context.stopService(serviceIntent);
        }
    }
}