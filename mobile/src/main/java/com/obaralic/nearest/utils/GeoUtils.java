package com.obaralic.nearest.utils;

import com.parse.ParseGeoPoint;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.PointF;

/**
 * Created by zivorad on 7/17/15.
 */
public class GeoUtils {

    public PointF calculateDerivedPosition(PointF point, double range, double bearing) {
        double EarthRadius = 6371000; // m

        double latA = Math.toRadians(point.x);
        double lonA = Math.toRadians(point.y);
        double angularDistance = range / EarthRadius;
        double trueCourse = Math.toRadians(bearing);

        double lat = Math.asin(Math.sin(latA) * Math.cos(angularDistance) +
            Math.cos(latA) * Math.sin(angularDistance) * Math.cos(trueCourse));

        double dlon = Math.atan2(Math.sin(trueCourse) * Math.sin(angularDistance) * Math.cos(latA),
            Math.cos(angularDistance) - Math.sin(latA) * Math.sin(lat));

        double lon = ((lonA + dlon + Math.PI) % (Math.PI * 2)) - Math.PI;

        lat = Math.toDegrees(lat);
        lon = Math.toDegrees(lon);

        PointF newPoint = new PointF((float) lat, (float) lon);

        return newPoint;
    }

    public ParseGeoPoint[] getGeoBoxCoordinates(Context context, ParseGeoPoint center) {
        SharedPreferences prefs = context.getSharedPreferences("TAG", Context.MODE_PRIVATE);
        float radius = prefs.getFloat("radius", 0);

        ParseGeoPoint[] geoBox = new ParseGeoPoint[2];
        PointF startPoint = new PointF((float) center.getLatitude(), (float) center.getLongitude());
        PointF southWest = calculateDerivedPosition(startPoint, radius, 225.0);
        PointF northEast = calculateDerivedPosition(startPoint, radius, 45.0);

        geoBox[0] = new ParseGeoPoint(southWest.x, southWest.y);
        geoBox[1] = new ParseGeoPoint(northEast.x, northEast.y);
        return geoBox;
    }
}
